import requests
from singer_sdk.authenticators import OAuthAuthenticator
from singer_sdk.helpers._util import utc_now

class iikoAuthenticator(OAuthAuthenticator):
    """API Authenticator for iiko flows."""

    @property
    def login_endpoint(self) -> str:
        return self.config.get("login_endpoint")

    @property
    def login_endpoint_params(self) -> dict:
        result = {"login": self.config.get("login_name"), "pass": self.config.get("login_password")}
        return result

    @property
    def logout_endpoint(self) -> str:
        return self.config.get("logout_endpoint")

    @property
    def auth_params(self) -> dict:
        result = super().auth_params
        result["key"] = self.access_token
        return result

    def update_access_token(self):
        """Update `access_token` along with: `last_refreshed` and `expires_in`."""
        request_time = utc_now()
        if not self.login_endpoint:
            self.logger.info("Login endpoint not set. Aborting.")
            return
        if not self.login_endpoint_params["login"]:
            self.logger.info("Login name not set. Aborting.")
            return
        if not self.login_endpoint_params["pass"]:
            self.logger.info("Login password not set. Aborting.")
            return
        token_response = requests.get(self.login_endpoint, params=self.login_endpoint_params, timeout=20)
        try:
            token_response.raise_for_status()
            self.logger.info("Login attempt was successful.")
            self.logger.info(f"token_response = {token_response.text}")
        except Exception as ex:
            raise RuntimeError(
                f"Failed login, response was '{token_response.text}'. {ex}"
            )
        token_json = token_response.text
        self.access_token = token_json
        self.last_refreshed = request_time

    def logout(self):
        if not self.logout_endpoint:
            self.logger.info("Logout endpoint not set. Aborting.")
            return
        self.logger.info(f"self.access_token = {self.access_token}")
        self.logger.info(f"auth_params = {self.auth_params}")
        if not self.auth_params["key"]:
            self.logger.info("Login key not set. Aborting.")
            return
        logout_response = requests.get(self.logout_endpoint, params=self.auth_params, timeout=20)
        try:
            logout_response.raise_for_status()
            self.logger.info("Logout attempt was successful.")
            self.logger.info(f"logout_response = {logout_response.text}")
        except Exception as ex:
            raise RuntimeError(
                f"Failed logout, response was '{logout_response.text}'. {ex}"
            )
