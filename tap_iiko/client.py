"""Custom client handling, including iikoStream base class."""

import requests
import copy
import re
import pendulum
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable, cast
from singer_sdk import typing as th  # JSON schema typing helpers
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.plugin_base import PluginBase as TapBaseClass

from datetime import datetime, timezone, timedelta
from singer_sdk.helpers._util import utc_now
from singer_sdk.streams.rest import RESTStream
from tap_iiko.iikoAuthenticator import iikoAuthenticator

import json

REQUEST_PAYLOADS_DIR = Path(__file__).parent / Path("./request_payloads")

# Example {{TODAY(%Y-%m-%d)}}
TODAY = "TODAY"
YESTERDAY = "YESTERDAY"
LAST_UPDATE = "LAST_UPDATE"

class iikoStream(RESTStream):

    name = "iiko"

    @property
    def url_base(self) -> str:
        return self.config.get("url_base")

    @property
    def path(self) -> str:
        return self.config.get("path")

    @property
    def rest_method(self) -> str:
        return self.config.get("rest_method")

    _schema = th.PropertiesList(
        th.Property("last_fetch_at", th.DateTimeType),
        th.Property("request", th.StringType),
        th.Property("response", th.ObjectType()),
    ).to_dict()

    replication_key = "last_fetch_at"

    @property
    def request_payload(self) -> str:
        return self.config.get("request_payload")

    @property
    def authenticator(self) -> Optional[iikoAuthenticator]:
        #if not self.authenticator:
        self._authenticator = iikoAuthenticator(stream=self)
        return self._authenticator# if not self.authenticator else self.authenticator

    def get_url(self, context: Optional[dict]) -> str:
        url = super().get_url(context)
        url = self.stringDatesReformatting(url)
        return url

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        if self.request_payload:

            REQUEST_PAYLOADS_DIR
            request_payload_filename = REQUEST_PAYLOADS_DIR / self.request_payload
            payload = json.dumps(Path(request_payload_filename).read_text())
            # self.logger.info(f"--{payload}--")
            payload = self.stringDatesReformatting(payload)
            # self.logger.info(f"--{payload}--")

            return json.loads(payload)[0]
        else:
            return {}

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        now = datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%S%Z")
        yield {"last_fetch_at": now, "request": self.get_url(context=None), "response": response.json()}


    def request_records(self, context: Optional[dict]) -> Iterable[dict]:
        """Request records from REST endpoint(s), returning response records.
        If pagination is detected, pages will be recursed automatically.
        """
        next_page_token: Any = None
        finished = False
        while not finished:
            prepared_request = self.prepare_request(
                context, next_page_token=next_page_token
            )
            resp = self._request_with_backoff(prepared_request, context)
            for row in self.parse_response(resp):
                yield row
            previous_token = copy.deepcopy(next_page_token)
            next_page_token = self.get_next_page_token(
                response=resp, previous_token=previous_token
            )
            if next_page_token and next_page_token == previous_token:
                raise RuntimeError(
                    f"Loop detected in pagination. "
                    f"Pagination token {next_page_token} is identical to prior token."
                )
            # Cycle until get_next_page_token() no longer returns a value
            finished = not next_page_token

        self._authenticator.logout()

    def stringDatesReformatting(self, input: str) -> str:
        splitted_input = re.split("{{|}}", input)
        result = ""
        for str in splitted_input:
            if str.startswith(TODAY):
                _, params = str.split("(")
                params = params.replace(")", "")
                formatted_res = datetime.now(timezone.utc).strftime(params)
                # print(formatted_res)
                result += formatted_res
            elif str.startswith(YESTERDAY):
                _, params = str.split("(")
                params = params.replace(")", "")
                formatted_res = formatted_res = (datetime.now(timezone.utc) - timedelta(days=1)).strftime(params)
                # print(formatted_res)
                result += formatted_res
            elif str.startswith(LAST_UPDATE):
                _, params = str.split("(")
                params = params.replace(")", "")
                replication_key_value = self.get_starting_replication_key_value
                last_update_datetime = cast(datetime.datetime, pendulum.parse(replication_key_value))
                formatted_res = last_update_datetime.strftime(params)
                # print(formatted_res)
                result += formatted_res
            else:
                result += str
        return result