"""iiko tap class."""

from typing import List

from singer_sdk import Tap, RESTStream
from singer_sdk import typing as th  # JSON schema typing helpers

# TODO: Import your custom stream types here:
from tap_iiko.client import iikoStream
# from tap_iiko.streams import (
#     iikoStream,
#     UsersStream,
#     GroupsStream,
# )
# TODO: Compile a list of custom stream types here
#       OR rewrite discover_streams() below with your custom logic.
# STREAM_TYPES = [
#     UsersStream,
#     GroupsStream,
# ]

# STREAM_TYPES = [
#     iikoStream
# ]

class Tapiiko(Tap):
    """iiko tap class."""
    name = "tap-iiko"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property("auth_token", th.StringType),
        th.Property("api_url", th.StringType, default="https://api.mysample.com"),
    ).to_dict()


    # @property
    # def catalog_dict(self) -> dict:
    #     return iikoStream.run_discovery(self.config)


    def discover_streams(self) -> List[RESTStream]:
        # return [stream_class(tap=self) for stream_class in STREAM_TYPES]
        """Return a list of discovered streams."""
        # pass
        # return [stream_class(tap=self) for stream_class in STREAM_TYPES]
        return [iikoStream(tap=self)]
